﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Controls;

namespace Battleship 
{
    [Serializable]
    public class Board : IEnumerable
    {

        private Square[,] SquareArray
        {
            set;
            get;
        }

        public Board()
        {
            SquareArray = new Square[10, 10];
            
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    SquareArray[i, j] = new Square();
                }
            }

        }

        public Square this[int x, int y]
        {
            get { return SquareArray[x, y]; }
        }

        public void Disable()
        {
            foreach (Square s in this)
            {
                s.BoardButton.IsEnabled = false;
            }
        }

        public void Enable()
        {
            foreach (Square s in this)
            {
                s.BoardButton.IsEnabled = true;
            }
        }

        public IEnumerator GetEnumerator()
        {
            return SquareArray.GetEnumerator();
        }
    }
}
