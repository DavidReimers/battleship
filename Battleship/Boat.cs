﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Battleship
{
    [Serializable]
    public class Boat
    {

        public int Length
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public int Hits
        {
            get;
            set;
        }

        public Boat(string n)
        {
            Name = n;

            switch (Name)
            {
                case "Carrier":
                    Length = 5;
                    break;
                case "Battleship":
                    Length = 4;
                    break;
                case "Destroyer":
                    Length = 3;
                    break;
                case "Submarine":
                    Length = 3;
                    break;
                case "Patrol Boat":
                    Length = 2;
                    break;
                default:
                    Length = 0;
                    break;
            }
        }

        public bool Sunk()
        {
            return Hits == Length;
        }

    }
}
