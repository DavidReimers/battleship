﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Battleship
{
    [Serializable]
    class ComputerPlayer : Player
    {

        private int targetX = 0, targetY = 0, xIncrement = 0, yIncrement = 0, currentDirection = 0;

        private int origX = 0, origY = 0;

        private bool target = false;

        private bool[] triedDirections = { false, false, false, false };
        public bool ComputerShoot(out Square s, out bool hit)
        {
            if (target)
            {

                if (targetY == 0)
                    triedDirections[0] = true;
                if (targetX == 9)
                    triedDirections[1] = true;
                if (targetY == 9)
                    triedDirections[2] = true;
                if (targetX == 0)
                    triedDirections[3] = true;

                if (triedDirections[currentDirection])
                {
                    targetX = origX;
                    targetY = origY;

                    if (currentDirection == 0)
                    {
                        if (triedDirections[2])
                            if (triedDirections[1])
                                currentDirection = 3;
                            else
                                currentDirection = 1;
                        else
                            currentDirection = 2;
                    }
                    else if (currentDirection == 2)
                    {
                        if (triedDirections[0])
                            if (triedDirections[1])
                                currentDirection = 3;
                            else
                                currentDirection = 1;
                        else
                            currentDirection = 0;
                    }
                    else if (currentDirection == 1)
                    {
                        if (triedDirections[3])
                            if (triedDirections[2])
                                currentDirection = 0;
                            else
                                currentDirection = 2;
                        else
                            currentDirection = 3;
                    }
                    else
                        currentDirection = 1;
                }

                if (currentDirection == 0)
                {
                    xIncrement = 0;
                    yIncrement = -1;
                }
                else if (currentDirection == 2)
                {
                    xIncrement = 0;
                    yIncrement = 1;
                }
                else if (currentDirection == 1)
                {
                    xIncrement = 1;
                    yIncrement = 0;
                }
                else
                {
                    xIncrement = -1;
                    yIncrement = 0;
                }

                s = Enemy.PlayerBoard[targetX + xIncrement, targetY + yIncrement];

                if (Enemy.PlayerBoard[targetX + xIncrement, targetY + yIncrement].Shoot())
                {
                    hit = true;

                    if (Enemy.PlayerBoard[targetX + xIncrement, targetY + yIncrement].BoatOnSquare.Sunk())
                    {
                        ClearTarget();
                        return true;
                    }
                    else
                    {
                        targetX += xIncrement;
                        targetY += yIncrement;
                    }
                }
                else
                {
                    hit = false;
                    triedDirections[currentDirection] = true;
                }
            }

            else
            {
                do
                {
                    targetX = BattleshipWindow.r.Next(10);
                    targetY = BattleshipWindow.r.Next(10);
                } while (Enemy.PlayerBoard[targetX, targetY].Shot);

                s = Enemy.PlayerBoard[targetX + xIncrement, targetY + yIncrement];
                
                if (Enemy.PlayerBoard[targetX, targetY].Shoot())
                {
                    target = true;
                    hit = true;

                    currentDirection = BattleshipWindow.r.Next(4);

                    origX = targetX;
                    origY = targetY;

                }
                else
                    hit = false;
            }

            if (triedDirections[0] && triedDirections[1] && triedDirections[2] && triedDirections[3])
                ClearTarget();

            return false;
        }

        public void ComputerPlaceShips()
        {
            bool boatPlaced = false;
            int x = 0, y = 0, xdiff, ydiff, dir = 0;

            for (int i = 0; i < 5; i++)
            {
                boatPlaced = false;

                while (!boatPlaced)
                {
                    x = BattleshipWindow.r.Next(10);
                    y = BattleshipWindow.r.Next(10);

                    dir = BattleshipWindow.r.Next(2);

                    if (dir == 0)
                    {
                        xdiff = 1;
                        ydiff = 0;
                    }
                    else
                    {
                        xdiff = 0;
                        ydiff = 1;
                    }

                    boatPlaced = true;

                    for (int j = 0; j < Fleet[i].Length && boatPlaced; j++)
                    {
                        if (x + xdiff * j >= 10 || y + ydiff * j >= 10)
                            boatPlaced = false;
                        else if (PlayerBoard[x + xdiff * j, y + ydiff * j].HasBoat)
                            boatPlaced = false;
                    }
                }

                PlaceBoat(Fleet[i], x, y, dir);
            }
        }

        public override void ClearTarget()
        {
            
            target = false;

            targetX = 0;
            targetY = 0;
            xIncrement = 0;
            yIncrement = 0;
            currentDirection = 0;
            origX = 0;
            origY = 0;

            for (int i = 0; i < triedDirections.Length; i++)
                triedDirections[i] = false;
            
        }

    }
}
