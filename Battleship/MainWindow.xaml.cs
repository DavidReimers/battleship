﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace Battleship
{
    /// <summary>
    /// Interaction logic for BattleshipWindow.xaml
    /// </summary>
    public partial class BattleshipWindow : Window
    {

        public static Random r = new Random();
        private bool boatLayingMode;
        private Player humanPlayer;
        private ComputerPlayer compPlayer;
        private bool gameStarted = false;
        private DispatcherTimer timer = new DispatcherTimer();

        public BattleshipWindow()
        {
            InitializeComponent();

            humanPlayer = new Player();
            compPlayer = new ComputerPlayer();

            humanPlayer.Enemy = compPlayer;
            compPlayer.Enemy = humanPlayer;

            timer.Interval = new TimeSpan(0, 0, 0, 0, 400);
            timer.Tick += new EventHandler(timer_Tick);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            Square shotSquare;
            bool hit;

            timer.Stop();

            if (compPlayer.ComputerShoot(out shotSquare, out hit))
            {
                playerStatusLabel.Text = string.Format("The Enemy sunk your {0}!", shotSquare.BoatOnSquare.Name);

                if (ActivePlayer.Enemy.AllBoatsSunk())
                    GameOver();
            }
            else
            {
                if (hit)
                    playerStatusLabel.Text = string.Format("The Enemy hit your {0}", shotSquare.BoatOnSquare.Name);
                else
                    playerStatusLabel.Text = string.Format("The Enemy missed");
            }

            ActivePlayer = humanPlayer;

            compPlayer.PlayerBoard.Enable();

            computerStatusLabel.Text = "Place your Shot on the Computer board";
        }

        private Player ActivePlayer
        {
            get;
            set;
        }

        private Player WhoStarts()
        {
            return (r.Next(2) == 0) ? humanPlayer : compPlayer;
        }

        private void NewGame()
        {
            gameStarted = false;
            saveGameButton.IsEnabled = false;

            compPlayer.Reset();
            humanPlayer.Reset();

            UpdateLayout();

            playerStatusLabel.Text = "";
            computerStatusLabel.Text = "";

            compPlayer.ComputerPlaceShips();

            ActivePlayer = WhoStarts();

            compPlayer.PlayerBoard.Disable();
            humanPlayer.PlayerBoard.Enable();

            playerStatusLabel.Text = "Place your Carrier on the board\n(right mouse button to change direction)";

            computerStatusLabel.Text = "The Enemy is placing their ships";

            boatLayingMode = true;

        }

        private void GameOver()
        {
            foreach (Square s in compPlayer.PlayerBoard)
            {
                if (s.HasBoat && !s.Shot)
                    s.BoardButton.Content = Square.Red;
            }
            gameStarted = false;

            string winner = (ActivePlayer == humanPlayer) ? "You are the winner" : "The Enemy has defeated you";
            NewDialog gameOver = new NewDialog("Game Over", winner, "OK", "Disable");

            gameOver.Owner = this;
            gameOver.ShowDialog();

            NewDialog playAgain = new NewDialog("Play Again", "Do you want to play again?", "Yes", "No");

            playAgain.Owner = this;
            playAgain.ShowDialog();


            if (playAgain.Number == 1)
                NewGame();
            else
                this.Close();
        }

        private void ComputerBoard_Click(object sender, RoutedEventArgs e)
        {
            Square clickedSquare = (from Square s in compPlayer.PlayerBoard where s.BoardButton == (Button)sender select s).First();

            if (!clickedSquare.Shot)
            {
                if (clickedSquare.Shoot())
                {
                    if (clickedSquare.BoatOnSquare.Sunk())
                    {
                        computerStatusLabel.Text = string.Format("You sank a {0}!", clickedSquare.BoatOnSquare.Name);
                        if (ActivePlayer.Enemy.AllBoatsSunk())
                            GameOver();
                    }
                    else
                        computerStatusLabel.Text = string.Format("You hit a {0}!", clickedSquare.BoatOnSquare.Name);
                }
                else
                    computerStatusLabel.Text = "You missed!";

                ActivePlayer = compPlayer;

                ComputerShoots();
            }
      }

        private void ComputerShoots()
        {
            compPlayer.PlayerBoard.Disable();
            timer.Start();
            playerStatusLabel.Text = "The Enemy is shooting at your fleet";
        }

        private void Battleship_Loaded(object sender, RoutedEventArgs e)
        {
            int count = 0;

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (boardGrid.Children[count] is Button)
                    {
                        humanPlayer.PlayerBoard[i, j].BoardButton = (Button)boardGrid.Children[count];

                        humanPlayer.PlayerBoard[i, j].BoardButton.Content = Square.Blank;
                    }

                    count++;
                }
            }

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (boardGrid.Children[count] is Button)
                    {
                        compPlayer.PlayerBoard[i, j].BoardButton = (Button)boardGrid.Children[count];

                        compPlayer.PlayerBoard[i, j].BoardButton.Content = Square.Blank;
                    }

                    count++;
                }
            }

            NewGame();
        }

        private void PlayerBoard_Click(object sender, RoutedEventArgs e)
        {
            if (boatLayingMode)
            {
                int xdiff, ydiff, direction;
                bool boatPlaced = true;

                if (humanPlayer.Horizontal)
                {
                    xdiff = 1;
                    ydiff = 0;
                    direction = 0;
                }
                else
                {
                    xdiff = 0;
                    ydiff = 1;
                    direction = 1;
                }

                int[] coordinates = GetCoordinates((Button)sender, humanPlayer);

                if (coordinates[0] + xdiff * (humanPlayer.Fleet[humanPlayer.CurrentBoat].Length - 1) < 10 && coordinates[1] + ydiff * (humanPlayer.Fleet[humanPlayer.CurrentBoat].Length - 1) < 10)
                {
                    for (int j = 0; j < humanPlayer.Fleet[humanPlayer.CurrentBoat].Length; j++)
                    {
                        if (humanPlayer.PlayerBoard[coordinates[0] + xdiff * j, coordinates[1] + ydiff * j].HasBoat)
                            boatPlaced = false;
                    }

                    if (boatPlaced)
                    {
                        gameStarted = true;
                        saveGameButton.IsEnabled = true;

                        humanPlayer.PlaceBoat(humanPlayer.Fleet[humanPlayer.CurrentBoat], coordinates[0], coordinates[1], direction);

                        humanPlayer.CurrentBoat++;

                        if (humanPlayer.CurrentBoat == 5)
                        {
                            boatLayingMode = false;
                            playerStatusLabel.Text = "";
                            computerStatusLabel.Text = "";

                            humanPlayer.PlayerBoard.Disable();

                            if (ActivePlayer == compPlayer)
                            {
                                ComputerShoots();
                            }
                            else
                            {
                                compPlayer.PlayerBoard.Enable();

                                computerStatusLabel.Text = "Place your Shot on the Computer board";
                            }

                        }
                        else
                        {
                            playerStatusLabel.Text = string.Format("Place your {0} on the board\n(right mouse button to change direction)", humanPlayer.Fleet[humanPlayer.CurrentBoat].Name);

                        }

                    }
                }
            }
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            if (boatLayingMode)
            {

                int xdiff, ydiff;

                if (humanPlayer.Horizontal)
                {
                    xdiff = 1;
                    ydiff = 0;
                }
                else
                {
                    xdiff = 0;
                    ydiff = 1;
                }

                int[] coordinates = GetCoordinates((Button)sender, humanPlayer);

                for (int k = 0; k < humanPlayer.Fleet[humanPlayer.CurrentBoat].Length && coordinates[0] + xdiff * k < 10 && coordinates[1] + ydiff * k < 10; k++)
                {
                    humanPlayer.PlayerBoard[coordinates[0] + xdiff * k, coordinates[1] + ydiff * k].BoardButton.Content = Square.Red;
                }
            }
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            if (boatLayingMode)
            {
                int xdiff, ydiff;

                if (humanPlayer.Horizontal)
                {
                    xdiff = 1;
                    ydiff = 0;
                }
                else
                {
                    xdiff = 0;
                    ydiff = 1;
                }

                int[] coordinates = GetCoordinates((Button)sender, humanPlayer);

                for (int k = 0; k < humanPlayer.Fleet[humanPlayer.CurrentBoat].Length && coordinates[0] + xdiff * k < 10 && coordinates[1] + ydiff * k < 10; k++)
                {
                    if (!humanPlayer.PlayerBoard[coordinates[0] + xdiff * k, coordinates[1] + ydiff * k].HasBoat)
                        humanPlayer.PlayerBoard[coordinates[0] + xdiff * k, coordinates[1] + ydiff * k].BoardButton.Content = Square.Blank;
                }
            }
        }

        private void bs_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (boatLayingMode)
            {
                if (humanPlayer.Horizontal)
                    humanPlayer.Horizontal = false;
                else
                    humanPlayer.Horizontal = true;

                foreach (Square s in humanPlayer.PlayerBoard)
                {
                    if (!s.HasBoat)
                        s.BoardButton.Content = Square.Blank;
                }

                if (sender is Button)
                    Button_MouseEnter(sender, e);
            }

        }

        private int[] GetCoordinates(Button b, Player p)
        {
            int[] c = new int[2];

            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    if (b == p.PlayerBoard[x, y].BoardButton)
                    {
                        c[0] = x;
                        c[1] = y;
                    }
                }
            }

            return c;
        }

        private void helpButton_Click(object sender, RoutedEventArgs e)
        {
            Helpfile help = new Helpfile();
            help.Owner = this;

            help.ShowDialog();
        }

        private void loadButton_Click(object sender, RoutedEventArgs e)
        {
            if (gameStarted)
                if (ContinueGame())
                    return;

            OpenFileDialog openFileDialog1 = new OpenFileDialog()
            {
                Filter = "Saved game files (*.sav)|*.sav",
                FilterIndex = 1,
                InitialDirectory = System.Reflection.Assembly.GetEntryAssembly().Location,
                CheckFileExists = true,
                Multiselect = false,
                Title = "Load Game",
                CheckPathExists = true
            };

            try
            {
                if ((bool)openFileDialog1.ShowDialog())
                {

                    BinaryFormatter binFormat = new BinaryFormatter();

                    using (Stream fStream = new FileStream(openFileDialog1.FileName, FileMode.Open, FileAccess.Read, FileShare.None))
                    {
                        Object[] loadObjects = (Object[])binFormat.Deserialize(fStream);

                        humanPlayer = (Player)loadObjects[0];
                        compPlayer = (ComputerPlayer)loadObjects[1];

                        GameStatus gs = (GameStatus)loadObjects[2];

                        ActivePlayer = gs.ap;
                        boatLayingMode = gs.blm;
                        gameStarted = gs.gs;

                        foreach (Square s in humanPlayer.PlayerBoard)
                        {
                            s.BoardButton = (Button)boardGrid.Children[s.Buttonref];

                            if (s.HasBoat)
                            {
                                s.BoardButton.Content = Square.Red;
                            }
                            else
                            {
                                s.BoardButton.Content = Square.Blank;
                            }

                            if (s.Shot)
                            {
                                if (s.HasBoat)
                                    s.BoardButton.Content = Square.Hit;
                                else
                                    s.BoardButton.Content = Square.Miss;
                            }
                        }

                        foreach (Square s in compPlayer.PlayerBoard)
                        {
                            s.BoardButton = (Button)boardGrid.Children[s.Buttonref];

                            s.BoardButton.Content = Square.Blank;

                            if (s.Shot)
                            {
                                if (s.HasBoat)
                                    s.BoardButton.Content = Square.Hit;
                                else
                                    s.BoardButton.Content = Square.Miss;
                            }
                        }
                    }

                    if (boatLayingMode)
                    {
                        humanPlayer.PlayerBoard.Enable();
                        compPlayer.PlayerBoard.Disable();
                        computerStatusLabel.Text = "The Enemy is placing their ships";
                        playerStatusLabel.Text = string.Format("Place your {0} on the board\n(right mouse button to change direction)", humanPlayer.Fleet[humanPlayer.CurrentBoat].Name);
                    }
                    else
                    {
                        humanPlayer.PlayerBoard.Disable();
                        playerStatusLabel.Text = "";

                        if (ActivePlayer == compPlayer)
                        {
                            compPlayer.PlayerBoard.Disable();

                            ComputerShoots();
                        }
                        else 
                        { 
                            compPlayer.PlayerBoard.Enable();

                            computerStatusLabel.Text = "Place your Shot on the Computer board";
                        }

                    }

                }
            }

            catch
            {
                NewDialog error = new NewDialog("File Error", "There was an error loading from the\nselected file. Please try again.", "OK", "Disable");
                error.Owner = this;

                error.ShowDialog();
            }

        }

        private bool ContinueGame()
        {
            NewDialog continueGame = new NewDialog("Load Game", "Are you sure you want to\nabandon the current game?", "Yes", "No");

            continueGame.Owner = this;
            continueGame.ShowDialog();

            return (continueGame.Number == 0);
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            if (gameStarted)
                if (ContinueGame())
                    return;

            NewGame();

        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {

            SaveFileDialog saveFileDialog1 = new SaveFileDialog()
            {
                Filter = "Saved game files (*.sav)|*.sav",
                FileName = string.Format("Savegame_{0}", DateTime.Now.ToShortDateString()),
                DefaultExt = ".sav",
                FilterIndex = 1,
                InitialDirectory = Assembly.GetEntryAssembly().Location,
                OverwritePrompt = true,
            };

            try
            {
                if ((bool)saveFileDialog1.ShowDialog())
                {
                    BinaryFormatter binFormat = new BinaryFormatter();

                    using (Stream fStream = new FileStream(saveFileDialog1.FileName, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        Object[] saveObjects = new Object[3];
                        saveObjects[0] = humanPlayer;
                        saveObjects[1] = compPlayer;

                        saveObjects[2] = new GameStatus { blm = boatLayingMode, ap = ActivePlayer, gs = gameStarted };

                        binFormat.Serialize(fStream, saveObjects);

                    }
                }
            }

            catch
            {
                NewDialog error = new NewDialog("File Error", "There was an error saving to the\nselected file. Please try again.", "OK", "Disable");
                error.Owner = this;

                error.ShowDialog();
            }

        }

        [Serializable]
        class GameStatus
        {
            internal bool blm;
            internal Player ap;
            internal bool gs;
        }

        private void bs_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (gameStarted)
            {
                NewDialog quit = new NewDialog("Quit without saving?", "Are you sure you want to\nquit without saving?", "Quit", "Save Game");

                quit.Owner = this;
                quit.ShowDialog();

                if (quit.Number == 0)
                    saveButton_Click(sender, new RoutedEventArgs());
            }
        }
    }
}
