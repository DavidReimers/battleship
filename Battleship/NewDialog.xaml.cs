﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Battleship
{
    /// <summary>
    /// Interaction logic for NewDialog.xaml
    /// </summary>
    public partial class NewDialog : Window
    {
        public int Number
        {
            get;
            private set;
        }
        
        public NewDialog(string title, string labeltext, string button1text, string button2text)
        {
            InitializeComponent();

            Title = title;
            textLabel.Text = labeltext;

            if (button1text == "Disable")
            {
                button1.Visibility = Visibility.Hidden;
                button2.Margin = new Thickness(113,83,0,0);
            }
            else
                button1.Content = button1text;

            if (button2text == "Disable")
            {
                button2.Visibility = Visibility.Hidden;
                button1.Margin = new Thickness(113,83,0,0);
            }
            else
                button2.Content = button2text;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            Number = 1;

            this.Close();

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Number = 0;
            this.Close();

        }
    }
}
