﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Battleship
{
    [Serializable]
    public class Player
    {

        private Boat[] fleet = { new Boat("Carrier"), new Boat("Battleship"), new Boat("Submarine"), new Boat("Destroyer"), new Boat("Patrol Boat") };

        public int CurrentBoat
        {
            get;
            set;
        }

        public bool Horizontal
        {
            get;
            set;
        }
        
        public Player Enemy
        {
            get;
            set;
        }

        public Boat[] Fleet
        {
            get
            {
                return fleet;
            }
        }

        public Board PlayerBoard
        {
            get;
            private set;
        }

        public Player()
        {
            PlayerBoard = new Board();
            CurrentBoat = 0;
        }

        public void Reset()
        {
            foreach (Square s in PlayerBoard)
            {
                s.HasBoat = false;
                s.BoatOnSquare = null;
                s.Shot = false;
                s.BoardButton.Content = Square.Blank;
            }

            foreach (Boat b in Fleet)
            {
                b.Hits = 0;
            }

            CurrentBoat = 0;
            Horizontal = false;

            ClearTarget();

        }

        public virtual void ClearTarget() {}

        public void PlaceBoat(Boat b, int x, int y, int direction)
        {
            int deltax, deltay;

            if (direction == 0)
            {
                deltax = 1;
                deltay = 0;
            }
            else
            {
                deltay = 1;
                deltax = 0;
            }

            for (int j = 0; j < b.Length; j++)
            {
                PlayerBoard[x + deltax * j, y + deltay * j].HasBoat = true;
                PlayerBoard[x + deltax * j, y + deltay * j].BoatOnSquare = b;
                if (!(this is ComputerPlayer))
                    PlayerBoard[x + deltax * j, y + deltay * j].BoardButton.Content = Square.Red;
            }
        }

        public bool AllBoatsSunk()
        {
            foreach (Boat b in Fleet)
            {
                if (!b.Sunk())
                    return false;
            }

            return true;
        }
    }
}
