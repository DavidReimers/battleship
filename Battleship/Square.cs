﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Runtime.Serialization;


namespace Battleship
{
    [Serializable]
    public class Square
    {
        [NonSerialized]
        private Button but;

        public int Buttonref 
        { 
            get; 
            set; 
        }

        public static SquareContent Red 
        {
            get { return new SquareContent("red"); }
        }

        public static SquareContent Blank
        {
            get { return new SquareContent("blank"); }
        }

        public static SquareContent Hit
        {
            get { return new SquareContent("hit"); }
        }

        public static SquareContent Miss
        {
            get { return new SquareContent("miss"); }
        }

        public bool HasBoat
        {
            get;
            set;
        }

        public bool Shot
        {
            get;
            set;
        }

        public Button BoardButton
        {
            get { return but; }
            set { but = value; }
        }

        public Boat BoatOnSquare
        {
            get;
            set;
        }

        public bool Shoot()
        {
            Shot = true;

            if (HasBoat)
            {
                BoatOnSquare.Hits++;
                
                BoardButton.Content = Hit;
                
                return true;
            }
            else
            {
                BoardButton.Content = Miss;

                return false;
            }
            
        }

        [OnSerializing]
        private void OnSerializing(StreamingContext context) 
        {
            Grid g = (Grid) BoardButton.Parent;

            Buttonref = g.Children.IndexOf(BoardButton);
        }

    }
}
