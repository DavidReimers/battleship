﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;

namespace Battleship
{
    public class SquareContent : Canvas
    {

        public SquareContent(string sign)
        {
            
            Height = 38;
            Width = 38;
            
            if (sign == "hit")
            {
                Line first = new Line()
                {
                    Stroke = Brushes.Black,
                    StrokeEndLineCap = PenLineCap.Round,
                    StrokeStartLineCap = PenLineCap.Round,

                    X1 = 4,
                    X2 = 34,
                    Y1 = 4,
                    Y2 = 34,

                    StrokeThickness = 5
                };

                Line second = new Line()
                {
                    Stroke = Brushes.Black,

                    X1 = 34,
                    X2 = 4,
                    Y1 = 4,
                    Y2 = 34,

                    StrokeEndLineCap = PenLineCap.Round,
                    StrokeStartLineCap = PenLineCap.Round,

                    StrokeThickness = 5
                };

                Background = Brushes.Crimson;

                this.Children.Add(first);
                this.Children.Add(second);

            }

            else if (sign == "miss")
            {
                Ellipse circle = new Ellipse()
                {
                    Stroke = Brushes.Black,
                    StrokeThickness = 5,
                    
                    Height = 38,
                    Width = 38
                };

                Background = Brushes.LightGray;

                this.Children.Add(circle);
            }
            
            else if (sign == "red")
            {
                Background = Brushes.Crimson;
            }

            else
            {
                Background = new SolidColorBrush(Color.FromArgb(255,40,40,230));
            }
        }
    }
}
